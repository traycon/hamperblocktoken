# HamperBlockToken

Total Fees by default: 2%
- 1% is reflected to holders
- 1% is added to LP

This token has a maxTX of 0.2% of total supply, this ensures that a whale can't move the price in 1TX.

